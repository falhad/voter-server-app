package ir.arcxx.voterserver;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.http.server.AsyncHttpServer;
import com.koushikdutta.ion.Ion;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollection;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import ir.arcxx.voterserver.adapter.QuestionAdapter;
import ir.arcxx.voterserver.model.Answer;
import ir.arcxx.voterserver.model.OnlineUsers;
import ir.arcxx.voterserver.model.Question;
import ir.arcxx.voterserver.model.Session;
import ir.arcxx.voterserver.model.User;
import ir.arcxx.voterserver.model.Vote;
import ir.arcxx.voterserver.utils.FontUtils;
import ir.arcxx.voterserver.utils.PrefUtils;
import ir.arcxx.voterserver.utils.WifiApController;

import static ir.arcxx.voterserver.utils.PrefUtils.getAppVersion;
import static ir.arcxx.voterserver.utils.PrefUtils.loadPref;


public class MainActivity extends AppCompatActivity {


    public static final String SESSION_KEY = "SESSION_KEY";
    public static final String DEVICE_ID = "DEVICE_ID";

    OrderedRealmCollection questions;

    public static final int ADD_NEW_QUESTION_REQUEST_CODE = 2017;
    String TAG = "MainActivity";
    Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;
    private ImageView add_new_question_iv;
    private TextView toolbar_title_tv;
    private ImageView reset_session_iv;
    private ImageView drawerIV;
    private Context context;
    private RecyclerView questionsRV;
    private RelativeLayout no_questions_exist_rl;
    private Button no_questions_exist_btn;
    private TextView no_questions_exist_tv;
    private View dialogServerStatusView;
    private ImageView server_status_iv;
    private TextView server_status_tv;
    private Button server_status_btn;
    private TextView online_users_tv;


    private boolean warning = true;
    private QuestionAdapter questionAdapter;
    private boolean serverAllowSendResult = false;


    private enum LoginStatus {SUCCESS}

    private AsyncHttpServer server;


    private Typeface IranSans;


    MaterialDialog serverStatusDialog;

    int port;
    RxPermissions rxPermissions;
    String after_they_vote;
    String after_admin_vote;
    Realm realm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        rxPermissions = new RxPermissions(this);

        realm = Realm.getDefaultInstance();


        after_they_vote = PrefUtils.loadPref(context, "after_they_vote");
        after_admin_vote = PrefUtils.loadPref(context, "after_admin_vote");

        if (after_they_vote == null || after_admin_vote == null) {
            PrefUtils.savePref(context, "after_they_vote", "false");
            PrefUtils.savePref(context, "after_admin_vote", "true");
        }


        String deviceID = PrefUtils.loadPref(context, DEVICE_ID);

        if (deviceID == null) {
            deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            PrefUtils.savePref(context, DEVICE_ID, deviceID);
        }
        realm.executeTransaction(realm -> {

            Session session = new Session(new Date());
            session = realm.copyToRealmOrUpdate(session);
            PrefUtils.savePref(context, SESSION_KEY, session.getId());
        });

        initElements();


    }

    public void turnOnHotspot(Context context) {
        Log.d(TAG, "turnOnHotspot: ");
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiApController apControl = WifiApController.getApControl(wifiManager);
        if (apControl != null) {

            // TURN OFF YOUR WIFI BEFORE ENABLE HOTSPOT
            if (wifiManager.isWifiEnabled()) {
                wifiManager.disconnect();
            }

            boolean isEnabled = apControl.setWifiApEnabled(apControl.getWifiApConfiguration(), true);

            Log.d(TAG, "turnOnHotspot: isEnabled " + isEnabled);

            startServer();

        }
    }

    private void turnApOn() {


        rxPermissions.request(Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET,
                Manifest.permission.CHANGE_WIFI_STATE
        )
                .subscribe(granted -> {
                    if (granted) {
                        turnOnHotspot(context);
                    } else {
                        Toast.makeText(context, "لطفا دسترسی های برنامه را چک کنید.", Toast.LENGTH_SHORT).show();
                        final Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + context.getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        context.startActivity(i);
                    }
                });


    }

    private void checkMasterPassword() {

        String successCheckValue = PrefUtils.loadPref(context, LoginStatus.SUCCESS.name());
        if (successCheckValue != null && successCheckValue.equalsIgnoreCase("true")) {
            //already logged in
            turnApOn();
        } else {

            MaterialDialog masterDialog = new MaterialDialog.Builder(this)
                    .title("کلمه عبور را وارد کنید.")
                    .titleGravity(GravityEnum.START)
                    .cancelable(false)
                    .customView(R.layout.master_password_layout, true)
                    .positiveText("تایید")
                    .negativeText("خروج")
                    .autoDismiss(false)
                    .onNegative((materialDialog, dialogAction) -> exitApplication())
                    .onPositive((materialDialog, dialogAction) -> {
                        View view = materialDialog.getCustomView();
                        EditText masterPwET = view.findViewById(R.id.remember_master_pw_et);
                        CheckBox masterPwCB = view.findViewById(R.id.remember_master_pw_cb);
                        TextView masterPwTV = view.findViewById(R.id.remember_master_pw_tv);

                        masterPwET.setTypeface(IranSans);
                        masterPwTV.setTypeface(IranSans);

                        String masterText = masterPwET.getText() + "";
                        if (masterText.isEmpty()) {
                            Toast.makeText(context, "کلمه عبور ضروری است.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (masterText.equalsIgnoreCase("C0mp96&)*") || masterText.equalsIgnoreCase("arcxx")) {
                            if (masterPwCB.isChecked()) {
                                PrefUtils.savePref(context, LoginStatus.SUCCESS.name(), "true");
                            } else {
                                PrefUtils.savePref(context, LoginStatus.SUCCESS.name(), "false");
                            }
                            turnApOn();
                            materialDialog.dismiss();
                        } else {
                            Toast.makeText(context, "کلمه عبور صحیح نیست.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    })
                    .build();

            masterDialog.show();

        }


    }

    private void exitApplication() {
        this.finishAffinity();
    }

    private void initElements() {
        IranSans = FontUtils.getTypeFace(context, FontUtils.IRANSans);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar_title_tv = findViewById(R.id.toolbar_title_tv);
        toolbar_title_tv.setTypeface(IranSans);

        coordinatorLayout = findViewById(R.id.coordinator);
        add_new_question_iv = findViewById(R.id.add_new_question_iv);
        reset_session_iv = findViewById(R.id.reset_session_iv);
        drawerIV = findViewById(R.id.drawer_iv);
        questionsRV = findViewById(R.id.questions_rv);
        no_questions_exist_rl = findViewById(R.id.no_questions_exist_rl);
        no_questions_exist_btn = findViewById(R.id.no_questions_exist_btn);
        no_questions_exist_tv = findViewById(R.id.no_questions_exist_tv);

        no_questions_exist_btn.setTypeface(IranSans);
        no_questions_exist_tv.setTypeface(IranSans);

        no_questions_exist_btn.setOnClickListener(v -> openAddNewQuestionActivity());
        add_new_question_iv.setOnClickListener(v -> openAddNewQuestionActivity());
        reset_session_iv.setOnClickListener(v -> new MaterialDialog.Builder(this)
                .titleGravity(GravityEnum.START)
                .title("آیا مایلید رای گیری کل سوالات از اول انجام شود؟")
                .positiveText("بله")
                .negativeText("خیر")
                .onNegative((dialog, which) -> dialog.dismiss())
                .onPositive((dialog, which) -> {
                    resetSession();
                })
                .show()
        );

        dialogServerStatusView = getLayoutInflater().inflate(R.layout.dialog_server_status, null);

        server_status_tv = dialogServerStatusView.findViewById(R.id.server_status_tv);
        server_status_btn = dialogServerStatusView.findViewById(R.id.server_status_btn);
        online_users_tv = dialogServerStatusView.findViewById(R.id.online_users_tv);

        server_status_tv.setTypeface(IranSans);
        server_status_btn.setTypeface(IranSans);
        online_users_tv.setTypeface(IranSans);
        server_status_btn.setOnClickListener(v -> startServer());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        questionsRV.setLayoutManager(layoutManager);


//        realm.addChangeListener(realm -> {
//
//            if (online_users_tv != null) {
//                online_users_tv.setText(String.format("تعداد افراد متصل : %s ", users.size() + ""));
//            }
////            if (questionAdapter != null) {
////                questionAdapter.notifyDataSetChanged();
////                questionAdapter.updateCounters();
////            }
//
////            if (questions.size() == 0) {
////                showNoQuestionExists();
////            } else {
////                showQuestionsRV();
////            }
//
//
//        });

        questions = realm.where(Question.class).equalTo("session.id", PrefUtils.loadPref(context, SESSION_KEY)).findAll();
        questionAdapter = new QuestionAdapter(questions, context);
        questionsRV.setAdapter(questionAdapter);


        initDrawer();
        checkMasterPassword();

    }

    private void resetSession() {

        questionAdapter.resetSession();
    }

    private void initDrawer() {

        PrimaryDrawerItem history = new PrimaryDrawerItem().withTag("history").withName("تاریخچه");
        PrimaryDrawerItem details = new PrimaryDrawerItem().withTag("details").withName("وضعیت سامانه");
        PrimaryDrawerItem settings = new PrimaryDrawerItem().withTag("settings").withName("تنظیمات");
        PrimaryDrawerItem about = new PrimaryDrawerItem().withTag("about").withName("درباره برنامه");
        PrimaryDrawerItem exit = new PrimaryDrawerItem().withTag("exit").withName("خروج");

        history.withTypeface(IranSans);
        details.withTypeface(IranSans);
        settings.withTypeface(IranSans);
        about.withTypeface(IranSans);
        exit.withTypeface(IranSans);

        //create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withDrawerGravity(Gravity.RIGHT)
                .withHeader(R.layout.header)
                .withHeaderPadding(false)
                .withSelectedItem(-1)
                .addDrawerItems(
                        history,
                        details,
                        settings,
                        about,
                        exit

                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    // do something with the clicked item :D
                    if (drawerItem.getTag().equals("history")) {
                        showHistory();
                    } else if (drawerItem.getTag().equals("details")) {
                        showServerStatus();
                    } else if (drawerItem.getTag().equals("settings")) {
                        showSettings();
                    } else if (drawerItem.getTag().equals("about")) {
                        showAbout();
                    } else if (drawerItem.getTag().equals("exit")) {
                        exitApplication();
                    } else if (drawerItem.getTag().equals("")) {

                    } else {

                    }
                    return false;
                })
                .build();


        drawerIV.setOnClickListener(v -> result.openDrawer());


    }

    private void showHistory() {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }

    private void showSettings() {

        MaterialDialog settingDialog = new MaterialDialog.Builder(this)
                .title("تنظیمات")
                .titleGravity(GravityEnum.START)
                .customView(R.layout.settings_dialog, false)
                .positiveText("خب")
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();

        RadioButton user_see_result_after_they_vote_rb = settingDialog.getCustomView().findViewById(R.id.user_see_result_after_they_vote_rb);
        RadioButton user_see_result_after_admin_vote_rb = settingDialog.getCustomView().findViewById(R.id.user_see_result_after_admin_vote_rb);

        user_see_result_after_they_vote_rb.setTypeface(IranSans);
        user_see_result_after_admin_vote_rb.setTypeface(IranSans);

        after_they_vote = PrefUtils.loadPref(context, "after_they_vote");
        after_admin_vote = PrefUtils.loadPref(context, "after_admin_vote");

        if (after_they_vote.equalsIgnoreCase("true") && after_admin_vote.equalsIgnoreCase("false")) {
            user_see_result_after_they_vote_rb.setChecked(true);
        } else {
            user_see_result_after_admin_vote_rb.setChecked(true);
        }


        user_see_result_after_they_vote_rb.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                PrefUtils.savePref(context, "after_they_vote", "true");
                PrefUtils.savePref(context, "after_admin_vote", "false");
            } else {
                PrefUtils.savePref(context, "after_they_vote", "false");
                PrefUtils.savePref(context, "after_admin_vote", "true");
            }
        });


    }

    private void showAbout() {


        MaterialDialog about = new MaterialDialog.Builder(context)
                .customView(R.layout.about, false)
                .positiveText("خب")
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();

        ViewGroup viewGroup = (ViewGroup) about.getCustomView();
        overrideFonts(context, viewGroup);
    }

    private void showServerStatus() {

        if (online_users_tv != null) {

            realm.executeTransaction(realm1 -> {
                OnlineUsers onlineUsers = realm1.where(OnlineUsers.class).equalTo("id", loadPref(context, SESSION_KEY)).findFirst();
                Log.d(TAG, "showServerStatus: onlineUsers" + onlineUsers.getUsers().toString());
                online_users_tv.setText(String.format("تعداد افراد متصل : %s ", onlineUsers.getUsers().size() + ""));
            });
        }

        serverStatusDialog = new MaterialDialog.Builder(context)
                .customView(dialogServerStatusView, true)
                .show();

    }

    private void openAddNewQuestionActivity() {
        Intent intent = new Intent(MainActivity.this, AddNewQuestionActivity.class);
        startActivityForResult(intent, ADD_NEW_QUESTION_REQUEST_CODE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkUIAdapter();
    }


    private void stopServer() {
        try {
            if (server != null) {
                AsyncServer.getDefault().stop();
                server.stop();
                server = null;
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
                Log.d(TAG, "stopServer: " + e.getMessage());
            } else {
                Log.d(TAG, "stopServer: ");
            }
        }
    }

    ReentrantLock reentrantLock = new ReentrantLock();

    private void startServer() {

        stopServer();

        realm.executeTransaction(transaction -> {

            transaction.where(OnlineUsers.class).findAll().deleteAllFromRealm();
            OnlineUsers onlineUsers = new OnlineUsers();
            onlineUsers.setId(PrefUtils.loadPref(context, SESSION_KEY));
            transaction.copyToRealmOrUpdate(onlineUsers);

        });

        server = new AsyncHttpServer();

        server.setErrorCallback(ex -> Toast.makeText(context, "مشکلی در راه اندازی سرور به وجود آمد." + ex.getMessage() + "", Toast.LENGTH_SHORT).show());

        server.post("/connect", (request, response) -> {
            Log.d(TAG, "/connect 1: " + request.getBody().get().toString());


            reentrantLock.lock();

            Realm connectRealm = Realm.getDefaultInstance();

            try {
                connectRealm.executeTransaction(transaction -> {
                    try {
                        User clientUser = new ObjectMapper().readValue(request.getBody().get().toString(), User.class);
                        Log.d(TAG, "startServer: ClientUser" + clientUser);
                        User user = transaction.where(User.class).equalTo("deviceID", clientUser.getDeviceID()).findFirst();
                        Log.d(TAG, "startServer: User" + user);
                        if (user == null) {
                            user = new User(clientUser.getDeviceID());
                            user = transaction.copyToRealmOrUpdate(user);
                            Log.d(TAG, "startServer: Null User " + user);
                        }

                        OnlineUsers onlineUsers = transaction.where(OnlineUsers.class).equalTo("id", loadPref(context, SESSION_KEY)).findFirst();
                        if (!onlineUsers.getUsers().contains(user)) {
                            Log.d(TAG, "startServer: User didn't exits! we added it!: ");
                            onlineUsers.getUsers().add(user);
                            Log.d(TAG, "startServer: onlineUsers" + onlineUsers.getUsers().toString());
                        }

                        response.send("200");

                    } catch (Exception e) {
                        e.printStackTrace();
                        response.send("ERROR ACCURED ON CONNECT ");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage() != null) {
                    response.send("ERROR " + e.getMessage());
                } else {
                    response.send("ERROR");
                }
            } finally {
                connectRealm.close();
            }

            reentrantLock.unlock();

        });

        server.post("/submit", (request, response) -> {
            Log.d(TAG, "/submit 1: " + request.getBody().get().toString());


            reentrantLock.lock();

            Realm submitRealm = Realm.getDefaultInstance();
            try {
                submitRealm.executeTransaction(transaction -> {
                    try {
                        Vote vote = new ObjectMapper().readValue(request.getBody().get().toString(), Vote.class);
                        Log.d(TAG, "startServer: VOTE " + vote.toString());
                        User user = transaction.where(User.class).equalTo("deviceID", vote.getUser().getDeviceID()).findFirst();
                        if (user == null) {
                            response.send("USER NOT FOUND");
                            return;
                        }

                        // TODO: 2017-08-27 age ghablan vote dade user be in answer dige natune bede
//                        Vote v = transaction.where(Vote.class).equalTo("",vote.getQuestionID())


                        Log.d(TAG, "startServer: USER " + user.toString());
                        Log.d(TAG, "submit: " + "Vote is " + vote.toString());
                        Log.d(TAG, "submit: " + "User is " + user.toString());
                        transaction.copyToRealmOrUpdate(vote);
                        //update answer voter!
                        Answer answer = transaction.where(Answer.class).equalTo("id", vote.getAnswerID()).findFirst();
                        Log.d(TAG, "startServer: Answer is " + answer.toString());
                        answer.getVoters().add(user);
                        Log.d(TAG, "startServer: Answer After update  is " + answer.toString());

                        response.send("200");
                    } catch (Exception e) {
                        e.printStackTrace();
                        response.send("ERROR ACCURED ON SUBMIT VOTE ");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage() != null) {
                    response.send("ERROR " + e.getMessage());
                } else {
                    response.send("ERROR");
                }
            } finally {
                submitRealm.close();
            }

            reentrantLock.unlock();

        });

        server.post("/refresh", (request, response) -> {

            Log.d(TAG, "/refresh 1: " + request.getBody().get().toString());


            reentrantLock.lock();

            Realm refreshRealm = Realm.getDefaultInstance();

            try {
                refreshRealm.executeTransaction(transaction -> {
                    try {

                        User clientUser = new ObjectMapper().readValue(request.getBody().get().toString(), User.class);
                        Log.d(TAG, "startServer: clientUser " + clientUser.toString());
                        User user = transaction.where(User.class).equalTo("deviceID", clientUser.getDeviceID()).findFirst();
                        if (user == null) {
                            response.send("USER NOT FOUND");
                            return;
                        }
                        Log.d(TAG, "startServer: user " + user.toString());

                        RealmResults<Question> sessionQuestions = transaction.where(Question.class).equalTo("session.id", loadPref(context, SESSION_KEY)).findAll();
                        RealmResults<Vote> sessionVotes = transaction.where(Vote.class).equalTo("user.deviceID", user.getDeviceID()).findAll();

                        ArrayList<Question> userMustAnswerQuestions = new ArrayList<>(sessionQuestions);
                        ArrayList<Question> sessionQuestionThatUserVotedFor = new ArrayList<>();

                        Log.d(TAG, "/refresh: " + "Questions " + Arrays.toString(sessionQuestions.toArray()));
                        Log.d(TAG, "/refresh: " + "Votes " + Arrays.toString(sessionVotes.toArray()));
                        Log.d(TAG, "/refresh: " + "userMustAnswerQuestions Before Filter " + Arrays.toString(userMustAnswerQuestions.toArray()));


                        for (Question q : sessionQuestions) {
                            for (Vote v : sessionVotes) {
                                if (q.getId().equalsIgnoreCase(v.getQuestionID())) {
                                    sessionQuestionThatUserVotedFor.add(q);
                                }
                            }
                        }

                        userMustAnswerQuestions.removeAll(sessionQuestionThatUserVotedFor);

                        //soalayi ke user javab dadaro dar che surat befreste barash?
                        // dar surati ke admin aval ejaze dade bashe ke usera javabaro bebinan
                        // dar surati ke admin be soal javab dade bashe
                        // FIXME: 2017-08-25 to option bezaram ye gozine ke user betune fori natijaro bebine ya inke montazer bashe admin ham javab bede bad


                        after_they_vote = PrefUtils.loadPref(context, "after_they_vote");
                        after_admin_vote = PrefUtils.loadPref(context, "after_admin_vote");


                        if (after_they_vote.equalsIgnoreCase("true")) {
                            // niazi be admin nist ke vote bede , resulta mire barashun
                            Log.d(TAG, "startServer: come on if here!");
                            userMustAnswerQuestions.addAll(sessionQuestionThatUserVotedFor);

                        } else if (after_admin_vote.equalsIgnoreCase("true")) {
                            // hatman ham admin ham user bayad javab bedan ta baraye user bere
                            Log.d(TAG, "startServer: come on else ! ");

                            RealmResults<Vote> allVotes = transaction.where(Vote.class).findAll();
                            Log.d(TAG, "startServer: AllVotes" + allVotes.toString());

                            // oon soalayi add beshe barash ke admin ham javab dade
                            for (Question q : sessionQuestionThatUserVotedFor) {
                                //voti ro migire ke marbut be in soale o admin vote dade .
                                Vote v = transaction.where(Vote.class).equalTo("questionID", q.getId()).equalTo("user.deviceID", PrefUtils.loadPref(context, DEVICE_ID)).findFirst();
                                if (v != null) {
                                    Log.d(TAG, "startServer: Admin Vote Found! " + v.toString());
                                    userMustAnswerQuestions.add(q);
                                } else {
                                    Log.d(TAG, "startServer: Admin Didn;t vote for " + q.toString());
                                }
                            }

                        }


                        Log.d(TAG, "/refresh: " + "userMustAnswerQuestions After Filter " + Arrays.toString(userMustAnswerQuestions.toArray()));
                        String result = new ObjectMapper().writeValueAsString(userMustAnswerQuestions);
                        Log.d(TAG, "/refresh: " + "Result" + result);
                        response.send(result);

                    } catch (Exception e) {
                        e.printStackTrace();
                        response.send("ERROR ACCURED ON CONNECT ");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage() != null) {
                    response.send("ERROR " + e.getMessage());
                } else {
                    response.send("ERROR");
                }
            } finally {
                refreshRealm.close();
            }

            reentrantLock.unlock();
        });


        try

        {
            ServerSocket serverSocket = new ServerSocket(0);
            port = serverSocket.getLocalPort();
            serverSocket.close();
            serverSocket = null;

            server.listen(port);
            server_status_tv.setText(String.format("%s", port + ""));

            sendConnectRequest();


            new MaterialDialog.Builder(this)
                    .title("کلمه عبور سرور:")
                    .titleGravity(GravityEnum.START)
                    .content(port + "")
                    .contentGravity(GravityEnum.CENTER)
                    .positiveText("خب")
                    .onPositive((dialog, which) -> dialog.dismiss())
                    .show();

        } catch (
                IOException e)

        {
            e.printStackTrace();

            String error = "";
            if (e.getMessage() != null) {
                error = e.getMessage();
            } else {
                error = "UNKNOWN ERROR";
            }

            new MaterialDialog.Builder(this)
                    .title("مشکل در اجرای سرور")
                    .content(error)
                    .positiveText("خب")
                    .onPositive((dialog, which) -> {
                        startServer();
                        dialog.dismiss();
                    })
                    .show();
        }


    }

    public void checkUIAdapter() {
        if (questions != null) {
            if (questions.size() == 0) {
                showNoQuestionExists();
            } else {
                showQuestionsRV();
            }
        }
    }

    private void showQuestionsRV() {
        if (questionsRV != null && no_questions_exist_rl != null) {
            questionsRV.setVisibility(View.VISIBLE);
            no_questions_exist_rl.setVisibility(View.GONE);
        }
    }

    private void showNoQuestionExists() {
        if (questionsRV != null && no_questions_exist_rl != null) {
            questionsRV.setVisibility(View.GONE);
            no_questions_exist_rl.setVisibility(View.VISIBLE);
        }
    }

    private void sendConnectRequest() {

        String connectURL = "http://localhost:" + port + "/connect";
        Log.d(TAG, "sendConnectRequest: Try to send " + connectURL);

        User tempUser = new User(PrefUtils.loadPref(context, DEVICE_ID));


        Ion.with(context)
                .load("POST", connectURL)
                .setJsonPojoBody(tempUser)
                .asString()
                .withResponse()
                .setCallback((e, stringResponse) -> {


                    if (e != null) {
                        Log.d(TAG, "sendConnectRequest: ");
                        e.printStackTrace();
                        if (e.getMessage() != null) {

                            Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - ", e.getMessage()), Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - "), Snackbar.LENGTH_LONG).show();

                        }

                    } else {
                        Log.d(TAG, "sendConnectRequest: " + stringResponse.getResult());
                        if (stringResponse.getResult().equalsIgnoreCase("200")) {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s", "متصل"), Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s", "خطا در سرور"), Snackbar.LENGTH_LONG).show();
                        }
                    }

                });


    }

    public void submitVote(Vote vote) {

        String submitUrl = "http://localhost:" + port + "/submit";

        Log.d(TAG, "submitVote: " + submitUrl);
        vote.setUser(new User(PrefUtils.loadPref(context, DEVICE_ID)));

        Ion.with(context)
                .load("POST", submitUrl)
                .setJsonPojoBody(vote)
                .asString()
                .withResponse()
                .setCallback((e, stringResponse) -> {

                    if (e != null) {
                        if (e.getMessage() != null) {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - ", e.getMessage()), Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - "), Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Log.d(TAG, "submitVote: " + stringResponse.getResult());
                        if (!stringResponse.getResult().equalsIgnoreCase("200")) {
                            if (e.getMessage() != null) {

                                Snackbar.make(coordinatorLayout, "" + String.format("مشکلی در ثبت رای به وجود آمد. ", e.getMessage()), Toast.LENGTH_SHORT).show();
                            } else {
                                Snackbar.make(coordinatorLayout, "" + String.format("مشکلی در ثبت رای به وجود آمد. "), Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                });

    }

    private void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(IranSans);
                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                if (((TextView) v).getId() == R.id.version_tv) {
                    ((TextView) v).setText(String.format("نسخه برنامه : %s", getAppVersion(context) + ""));
                }

            }
        } catch (Exception e) {
        }
    }

}
