package ir.arcxx.voterserver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;


import io.realm.Realm;
import io.realm.RealmList;
import ir.arcxx.voterserver.model.Answer;
import ir.arcxx.voterserver.model.Question;
import ir.arcxx.voterserver.model.Session;
import ir.arcxx.voterserver.utils.FontUtils;
import ir.arcxx.voterserver.utils.PrefUtils;

import static ir.arcxx.voterserver.MainActivity.SESSION_KEY;


public class AddNewQuestionActivity extends AppCompatActivity {

    private static final String TAG = "AddNewQuestionActivity";
    private Context context;

    ImageView close_activity_iv;
    ImageView save_question_iv;
    ImageView template_question_iv;

    EditText question_title_field;
    Button add_new_answer_field;
    LinearLayout answers_section_ll;
    Question question;
    ArrayList<View> answerViews;
    ScrollView scroll_view;

    Typeface IranSans;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_question);
        context = this;
        IranSans = FontUtils.getTypeFace(context, FontUtils.IRANSans);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        answerViews = new ArrayList<>();
        realm = Realm.getDefaultInstance();


        close_activity_iv = findViewById(R.id.close_activity_iv);
        save_question_iv = findViewById(R.id.save_question_iv);
        template_question_iv = findViewById(R.id.template_question_iv);
        scroll_view = findViewById(R.id.scroll_view);
        question_title_field = findViewById(R.id.question_title_field);
        add_new_answer_field = findViewById(R.id.add_new_answer_field);
        answers_section_ll = findViewById(R.id.answers_section_ll);


        question_title_field.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));
        add_new_answer_field.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));


        add_new_answer_field.setOnClickListener(v -> {

            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            final View newAnswerItemView = layoutInflater.inflate(R.layout.new_answer_item, null);

            ImageView deleteField = (ImageView) newAnswerItemView.findViewById(R.id.delete_field);
            EditText answerField = (EditText) newAnswerItemView.findViewById(R.id.answer_field);

            answerField.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));

            answers_section_ll.addView(newAnswerItemView);
            answerViews.add(newAnswerItemView);
            scroll_view.post(() -> {
                scroll_view.fullScroll(ScrollView.FOCUS_DOWN);
                newAnswerItemView.requestFocus();
            });

            deleteField.setOnClickListener(v1 -> {
                answers_section_ll.removeView(newAnswerItemView);
                answerViews.remove(newAnswerItemView);
            });


        });


        close_activity_iv.setOnClickListener(v -> finish());
        save_question_iv.setOnClickListener(view -> saveAnswerAndBackToPreviousActivity());
        template_question_iv.setOnClickListener(v -> showTemplatesDialog());

        if (getIntent().getExtras() != null) {
            questionIDFromIntent = getIntent().getExtras().getString("question");
            if (questionIDFromIntent != null) {
                realm.executeTransaction(realm -> {
                    Question question = realm.where(Question.class).equalTo("id", questionIDFromIntent).findFirst();
                    if (question != null) {
                        fillTemplateWithQuestion(question);
                    }
                });
            }
        }

    }

    String questionIDFromIntent;

    private void fillTemplateWithQuestion(Question question) {

        question_title_field.setText(question.getTitle() + "");
        answers_section_ll.removeAllViews();
        answers_section_ll.invalidate();
        answerViews.clear();


        for (int j = 0; j < question.getAnswers().size(); j++) {
            add_new_answer_field.performClick();
        }

        for (int j = 0; j < answerViews.size(); j++) {
            ((TextView) answerViews.get(j).findViewById(R.id.answer_field)).setText(question.getAnswers().get(j).getTitle());
        }

    }

    private void showTemplatesDialog() {


        ArrayList<String> templateItems = new ArrayList<>();
        String yesNoSoSo = "بله/خیر/ممتنع";
        String chooseTeacher = "تعیین استاد درس ..";

        templateItems.add(yesNoSoSo);
        templateItems.add(chooseTeacher);


        new MaterialDialog.Builder(this)
                .title("نوع قالب سوال:")
                .titleGravity(GravityEnum.START)
                .items(templateItems)
                .typeface(IranSans, IranSans)
                .itemsGravity(GravityEnum.CENTER)
                .itemsCallback((materialDialog, view, i, charSequence) -> {

                    if (charSequence.toString().equalsIgnoreCase(yesNoSoSo)) {

                        answers_section_ll.removeAllViews();
                        answers_section_ll.invalidate();
                        answerViews.clear();

                        add_new_answer_field.performClick();
                        add_new_answer_field.performClick();
                        add_new_answer_field.performClick();


                        ((TextView) answerViews.get(0).findViewById(R.id.answer_field)).setText("بله");
                        ((TextView) answerViews.get(1).findViewById(R.id.answer_field)).setText("خیر");
                        ((TextView) answerViews.get(2).findViewById(R.id.answer_field)).setText("ممتنع");


                    } else if (charSequence.toString().equalsIgnoreCase(chooseTeacher)) {


                        question_title_field.setText("تعیین استاد درس ");
                        answers_section_ll.removeAllViews();
                        answers_section_ll.invalidate();
                        answerViews.clear();

                        add_new_answer_field.performClick();
                        add_new_answer_field.performClick();
                        add_new_answer_field.performClick();

                        for (int j = 0; j < answerViews.size(); j++) {
                            ((TextView) answerViews.get(j).findViewById(R.id.answer_field)).setText("دکتر ");
                        }


                    } else {


                    }


                })
                .show();

    }

    private void saveAnswerAndBackToPreviousActivity() {


        if ((question_title_field.getText() + "").isEmpty()) {
            Toast.makeText(context, "لطفا عنوان سوال را مطرح کنید.", Toast.LENGTH_SHORT).show();
            return;
        }


        // Realm  realm = Realm.getDefaultInstance();

        realm.executeTransaction(realm -> {

            Session session = realm.where(Session.class).equalTo("id", PrefUtils.loadPref(context, SESSION_KEY)).findFirst();
            question = new Question(session);
            RealmList<Answer> answers = new RealmList<>();

            question.setTitle(question_title_field.getText() + "");

            for (int i = 0; i < answerViews.size(); i++) {
                TextView answerTV = answerViews.get(i).findViewById(R.id.answer_field);
                Answer ans = new Answer(answerTV.getText() + "");
                answers.add(ans);
            }

            question.setAnswers(answers);

            if (answers.size() == 0) {
                Toast.makeText(context, "لطفا حداقل یک گزینه برای پاسخگویی مطرح کنید.", Toast.LENGTH_SHORT).show();
                return;
            }
            realm.copyToRealmOrUpdate(question);
            Log.d(TAG, "saveAnswerAndBackToPreviousActivity: "+question.toString());
            finish();
        });


    }


}
