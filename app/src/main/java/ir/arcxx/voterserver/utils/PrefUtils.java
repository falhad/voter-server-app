package ir.arcxx.voterserver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

/**
 * Created by Farhad on 1/20/2017.
 */

public class PrefUtils {


    public static String loadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String value = sharedPreferences.getString(key, null);
        System.out.println("Load Pref > " + context.getClass().getName() + ":" + key + ":" + value);
        return value;
    }


    public static void savePref(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        System.out.println("Save Pref > " + context.getClass().getName() + ":" + key + ":" + value);
        editor.commit();
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return pInfo.versionCode;
        } catch (Exception e) {
            return 0;
        }
    }

}


