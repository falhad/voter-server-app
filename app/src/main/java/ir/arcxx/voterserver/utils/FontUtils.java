package ir.arcxx.voterserver.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Farhad on 8/16/2015.
 */
public class FontUtils {

    public static final String Bauhaus = "Bauhaus.ttf";
    public static final String IRANSans = "IRAN Sans.ttf";
    public static final String IRANSansBold = "IRAN Sans Bold.ttf";


    private static final Map<String, Typeface> FONTS = new HashMap<String, Typeface>();

    public static Typeface getTypeFace(Context context, String typeFaceName) {
        Typeface typeface = FONTS.get(typeFaceName);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + typeFaceName);
            FONTS.put(typeFaceName, typeface);
        }
        return typeface;
    }


    public static void setTypeFace(TextView view, String typeFaceName) {
        view.setTypeface(getTypeFace(view.getContext(), typeFaceName));
    }


}
