package ir.arcxx.voterserver.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import ir.arcxx.voterserver.AddNewQuestionActivity;
import ir.arcxx.voterserver.HistoryActivity;
import ir.arcxx.voterserver.MainActivity;
import ir.arcxx.voterserver.R;
import ir.arcxx.voterserver.model.Answer;
import ir.arcxx.voterserver.model.Question;
import ir.arcxx.voterserver.model.Vote;
import ir.arcxx.voterserver.utils.FontUtils;
import ir.arcxx.voterserver.utils.PrefUtils;

import static ir.arcxx.voterserver.MainActivity.DEVICE_ID;
import static ir.arcxx.voterserver.MainActivity.SESSION_KEY;


/**
 * Created by Farhad on 2017-03-07.
 */

public class QuestionAdapter extends RealmRecyclerViewAdapter<Question, QuestionAdapter.QuestionViewHolder> {

    Context context;
    ArrayList<QuestionAnswerAdapter> questionAnswerAdapters;
    String TAG = "Question Adapter";
    String deviceID;
    Typeface IranSans;
    Realm realm;

    public QuestionAdapter(OrderedRealmCollection<Question> questions, Context context) {
        super(questions, true);
        setHasStableIds(true);

        this.context = context;
        this.deviceID = PrefUtils.loadPref(context, DEVICE_ID);
        this.questionAnswerAdapters = new ArrayList<>();
        this.IranSans = FontUtils.getTypeFace(context, FontUtils.IRANSans);

    }


    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_item, parent, false);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {

        Question question = getItem(position);

        realm = Realm.getDefaultInstance();

        //title
        holder.title.setText(question.getTitle());
        holder.title.setTypeface(IranSans);

//        //totalVotes
//        realm.executeTransaction(realm -> {
//            int totalVotes = realm.where(Vote.class).equalTo("questionID", question.getId()).findAll().size();
//            holder.total_votes_tv.setText(String.format("%s : %s", "تعداد رای ها", totalVotes + ""));
//        });

        int totalVotes = 0;
        for (int i = 0; i < question.getAnswers().size(); i++) {
            totalVotes += question.getAnswers().get(i).getVoters().size();
        }
        holder.total_votes_tv.setText(String.format("%s : %s", "تعداد رای ها", totalVotes + ""));

        holder.total_votes_tv.setTypeface(IranSans);

        //answers
        QuestionAnswerAdapter questionAnswerAdapter = new QuestionAnswerAdapter(question, context);
        questionAnswerAdapters.add(position, questionAnswerAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        holder.answers.setLayoutManager(layoutManager);
        holder.answers.setAdapter(questionAnswerAdapter);




        //load More
        holder.more_iv.setOnClickListener(view -> {


            PopupMenu popup = new PopupMenu(context, holder.more_iv, Gravity.CENTER);
            popup.getMenu().add("حذف");
            if (context instanceof MainActivity) {
                popup.getMenu().add("رای گیری دوباره");
            }
            popup.getMenu().add("به عنوان سوال جدید");

            popup.setOnMenuItemClickListener(item -> {

                switch (item.getTitle() + "") {
                    case "حذف":

                        new MaterialDialog.Builder(context)
                                .titleGravity(GravityEnum.START)
                                .title("آیا مایلید این سوال حذف شود؟")
                                .positiveText("بله")
                                .negativeText("خیر")
                                .onNegative((dialog, which) -> dialog.dismiss())
                                .onPositive((dialog, which) -> deleteQuestion(question))
                                .show();

                        break;
                    case "رای گیری دوباره":
                        new MaterialDialog.Builder(context)
                                .titleGravity(GravityEnum.START)
                                .title("آیا مایلید این سوال از نو رای گیری شود؟")
                                .positiveText("بله")
                                .negativeText("خیر")
                                .onNegative((dialog, which) -> dialog.dismiss())
                                .onPositive((dialog, which) -> resetQuestion(question))
                                .show();
                        break;
                    case "به عنوان سوال جدید":
                        Intent intent = new Intent(context, AddNewQuestionActivity.class);
                        intent.putExtra("question", question.getId());
                        context.startActivity(intent);
                        break;


                    default:
                        return false;
                }
                return false;
            });

            popup.show();

        });


    }



    private void deleteQuestion(Question question) {
        realm.executeTransaction(realm -> {
            realm.where(Question.class).equalTo("id", question.getId()).findAll().deleteAllFromRealm();
            if (context instanceof MainActivity) {
                ((MainActivity) context).checkUIAdapter();
            } else if (context instanceof HistoryActivity) {
                ((HistoryActivity) context).checkUIAdapter();
            }
        });
    }

    private void resetQuestion(Question question) {
        realm.executeTransaction(realm -> {
            realm.where(Vote.class).equalTo("questionID", question.getId()).findAll().deleteAllFromRealm();
            for (Answer a : question.getAnswers()) {
                a.getVoters().clear();
            }
        });
    }

    public void resetSession() {

        realm.executeTransactionAsync(realm -> {
            List<Question> sessionQuestions = realm.where(Question.class).equalTo("session.id", PrefUtils.loadPref(context, SESSION_KEY)).findAll();
            for (Question q : sessionQuestions) {
                realm.where(Vote.class).equalTo("questionID", q.getId()).findAll().deleteAllFromRealm();
                for (Answer a : q.getAnswers()) {
                    a.getVoters().clear();
                }
            }

        });
    }


    class QuestionViewHolder extends RecyclerView.ViewHolder {

        CardView question_cv;
        TextView title;
        RecyclerView answers;
        ImageView more_iv;
        TextView total_votes_tv;

        QuestionViewHolder(View itemView) {
            super(itemView);
            question_cv = itemView.findViewById(R.id.question_cv);
            answers = itemView.findViewById(R.id.question_answer_rv);
            title = itemView.findViewById(R.id.question_title);
            more_iv = itemView.findViewById(R.id.more_iv);
            total_votes_tv = itemView.findViewById(R.id.total_votes_tv);
        }
    }


}
