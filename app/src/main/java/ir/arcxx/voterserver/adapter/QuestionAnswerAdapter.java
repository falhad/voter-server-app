package ir.arcxx.voterserver.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import ir.arcxx.voterserver.AddNewQuestionActivity;
import ir.arcxx.voterserver.HistoryActivity;
import ir.arcxx.voterserver.MainActivity;
import ir.arcxx.voterserver.R;
import ir.arcxx.voterserver.model.Answer;
import ir.arcxx.voterserver.model.Question;
import ir.arcxx.voterserver.model.User;
import ir.arcxx.voterserver.model.Vote;
import ir.arcxx.voterserver.utils.FontUtils;
import ir.arcxx.voterserver.utils.PrefUtils;

import static ir.arcxx.voterserver.MainActivity.DEVICE_ID;


/**
 * Created by Farhad on 2017-03-07.
 */

public class QuestionAnswerAdapter extends RealmRecyclerViewAdapter<Answer, QuestionAnswerAdapter.QuestionViewHolder> {

    Question question;
    private Context context;
    Typeface IranSans;
    String TAG = "QuestionAnswerAdapter";
    Realm realm;


    public QuestionAnswerAdapter(Question question, Context context) {
        super(question.getAnswers(), true);
        setHasStableIds(true);

        this.question = question;
        this.context = context;
        this.IranSans = FontUtils.getTypeFace(context, FontUtils.IRANSans);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.answers_item, parent, false);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {

        Answer answer = getItem(position);


        holder.answer_option_title_tv.setText(String.format("%s", answer.getTitle() + ""));
        holder.answer_option_title_tv.setTypeface(IranSans);

        holder.answer_vote_count_tv.setText(answer.getVoters().size() + "");
        holder.answer_vote_count_tv.setTypeface(IranSans);

        boolean showResult = false;


        List<Vote> sessionVotes = realm.where(Vote.class).equalTo("user.deviceID", PrefUtils.loadPref(context, DEVICE_ID)).findAll();

        for (Vote v : sessionVotes) {
            if (question.getId().equalsIgnoreCase(v.getQuestionID())) {
                showResult = true;
            }
        }

        if (showResult) {
            holder.answer_vote_selection_iv.setVisibility(View.INVISIBLE);
            holder.answer_vote_count_tv.setVisibility(View.VISIBLE);
        } else {
            holder.answer_vote_selection_iv.setVisibility(View.VISIBLE);
            holder.answer_vote_count_tv.setVisibility(View.INVISIBLE);

            if (context instanceof MainActivity) {


                holder.answer_rl.setOnClickListener(v -> {

                    holder.answer_vote_selection_iv.setBackgroundResource(R.drawable.selected);

                    Vote vote = new Vote(new User(PrefUtils.loadPref(context, DEVICE_ID)), question.getId(), answer.getId());

                    ViewGroup parentView = ((ViewGroup) holder.answer_rl.getParent());

                    for (int i = 0; i < parentView.getChildCount(); i++) {
                        parentView.getChildAt(i).setEnabled(false);
                    }

                    ((MainActivity) context).submitVote(vote);

                    Log.d(TAG, "onBindViewHolder: " + answer.toString());


                });


            }

        }

    }


    public class QuestionViewHolder extends RecyclerView.ViewHolder {

        TextView answer_vote_count_tv;
        TextView answer_option_title_tv;
        ImageView answer_vote_selection_iv;
        RelativeLayout answer_rl;

        public QuestionViewHolder(View itemView) {
            super(itemView);

            answer_vote_count_tv = itemView.findViewById(R.id.answer_vote_count_tv);
            answer_option_title_tv = itemView.findViewById(R.id.answer_option_title_tv);
            answer_vote_selection_iv = itemView.findViewById(R.id.answer_vote_selection_iv);
            answer_rl = itemView.findViewById(R.id.answer_rl);
        }
    }


}
