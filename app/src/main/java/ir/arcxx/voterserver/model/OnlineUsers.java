package ir.arcxx.voterserver.model;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Farhad on 2017-08-28.
 */
@RealmClass
public class OnlineUsers extends RealmObject implements RealmModel {

    @PrimaryKey
    String id;
    RealmList<User> users;


    public OnlineUsers() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<User> getUsers() {
        return users;
    }

    public void setUsers(RealmList<User> users) {
        this.users = users;
    }
}
