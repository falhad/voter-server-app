package ir.arcxx.voterserver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Farhad on 2017-03-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Question extends RealmObject {


    @PrimaryKey
    private String id;
    private Session session;
    private String title;
    private RealmList<Answer> answers;


    public Question() {
        this.id = UUID.randomUUID().toString();
    }

    public Question(Session session) {
        this.session = session;
        this.id = UUID.randomUUID().toString();
    }

    public Question(Session session, String title, RealmList<Answer> answers) {
        this.id = UUID.randomUUID().toString();
        this.session = session;
        this.title = title;
        this.answers = answers;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RealmList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(RealmList<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id='" + id + '\'' +
                ", session=" + session +
                ", title='" + title + '\'' +
                ", answers=" + answers +
                '}';
    }
}

