package ir.arcxx.voterserver.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.UUID;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Farhad on 2017-03-08.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vote extends RealmObject {

    @PrimaryKey
    private String id;
    User user;
    String questionID;
    String answerID;


    public Vote() {
        this.id = UUID.randomUUID().toString();
    }

    public Vote(User user, String questionID, String answerID) {
        this.id = UUID.randomUUID().toString();
        this.user = user;
        this.questionID = questionID;
        this.answerID = answerID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String getAnswerID() {
        return answerID;
    }

    public void setAnswerID(String answerID) {
        this.answerID = answerID;
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id='" + id + '\'' +
                ", user=" + user +
                ", questionID='" + questionID + '\'' +
                ", answerID='" + answerID + '\'' +
                '}';
    }
}
