package ir.arcxx.voterserver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import ir.arcxx.voterserver.adapter.QuestionAdapter;
import ir.arcxx.voterserver.model.Answer;
import ir.arcxx.voterserver.model.Question;
import ir.arcxx.voterserver.model.Session;
import ir.arcxx.voterserver.model.User;
import ir.arcxx.voterserver.model.Vote;
import ir.arcxx.voterserver.utils.FontUtils;
import ir.arcxx.voterserver.utils.PrefUtils;

import static ir.arcxx.voterserver.MainActivity.ADD_NEW_QUESTION_REQUEST_CODE;
import static ir.arcxx.voterserver.MainActivity.DEVICE_ID;


public class HistoryActivity extends AppCompatActivity {


    Toolbar toolbar;
    private ImageView close_iv;
    private ImageView clear_history_iv;
    private TextView toolbar_title_tv;
    private Context context;
    private RecyclerView questionsRV;

    boolean warning = true;


    private OrderedRealmCollection questions;

    private QuestionAdapter questionAdapter;

    RelativeLayout no_questions_exist_rl;
    Button no_questions_exist_btn;
    TextView no_questions_exist_tv;

    Typeface IranSans;

    Realm realm;
    private User deviceUser = new User();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);


        context = this;
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(realm -> {
            User user = realm.where(User.class).equalTo("deviceID", PrefUtils.loadPref(context, DEVICE_ID)).findFirst();
            if (user == null) {
                user = new User(PrefUtils.loadPref(context, DEVICE_ID));
                user = realm.copyToRealmOrUpdate(user);
            }
            deviceUser = user;
        });

        initElements();

    }


    private void initElements() {

        IranSans = FontUtils.getTypeFace(context, FontUtils.IRANSans);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        close_iv = findViewById(R.id.close_iv);
        toolbar_title_tv = findViewById(R.id.toolbar_title_tv);
        clear_history_iv = findViewById(R.id.clear_history_iv);
        toolbar_title_tv.setTypeface(IranSans);
        close_iv.setOnClickListener(view -> finish());
        questionsRV = findViewById(R.id.questions_rv);
        no_questions_exist_rl = findViewById(R.id.no_questions_exist_rl);
        no_questions_exist_btn = findViewById(R.id.no_questions_exist_btn);
        no_questions_exist_tv = findViewById(R.id.no_questions_exist_tv);


        no_questions_exist_btn.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));
        no_questions_exist_tv.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));

        no_questions_exist_btn.setOnClickListener(v -> {

            questions = realm.where(Question.class).findAll();
            if (questionAdapter != null) {
                questionAdapter.notifyDataSetChanged();
            }
            if (questions.size() == 0) {
                showNoQuestionExists();
            } else {
                showQuestionsRV();
            }

        });


        questions = realm.where(Question.class).findAll();


        questionAdapter = new QuestionAdapter(questions, context);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        questionsRV.setLayoutManager(layoutManager);
        questionsRV.setAdapter(questionAdapter);

        clear_history_iv.setOnClickListener(view ->
                new MaterialDialog.Builder(context)
                        .titleGravity(GravityEnum.START)
                        .title("آیا مایلید تاریخچه پاک شود؟")
                        .positiveText("بله")
                        .negativeText("خیر")
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .onPositive((dialog, which) -> clearHistory())
                        .show());


    }

    @Override
    protected void onResume() {
        super.onResume();
        checkUIAdapter();
    }


    private void clearHistory() {

        realm.executeTransaction(realm -> {
            realm.where(Vote.class).findAll().deleteAllFromRealm();
            realm.where(Question.class).findAll().deleteAllFromRealm();
            realm.where(Answer.class).findAll().deleteAllFromRealm();
            checkUIAdapter();
        });

    }


    public void checkUIAdapter() {
        if (questions != null) {
            if (questions.size() == 0) {
                showNoQuestionExists();
            } else {
                showQuestionsRV();
            }
        }
    }


    private void showQuestionsRV() {
        if (questionsRV != null && no_questions_exist_rl != null) {
            questionsRV.setVisibility(View.VISIBLE);
            no_questions_exist_rl.setVisibility(View.GONE);
        }
    }

    private void showNoQuestionExists() {
        if (questionsRV != null && no_questions_exist_rl != null) {
            questionsRV.setVisibility(View.GONE);
            no_questions_exist_rl.setVisibility(View.VISIBLE);
        }
    }


}
